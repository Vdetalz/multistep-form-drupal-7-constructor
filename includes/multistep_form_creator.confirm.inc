<?php

/**
 * @file
 * Handles the form confirmation of the multistep form creator form.
 */

/**
 * Confirm data for the confirm step.
 */
function multistep_form_creator_confirm_details_form($form, &$form_state, $i) {
  $values    = isset($form_state['multistep_values']) ? $form_state['multistep_values'] : [];
  $separator = variable_get('title_separator');

  // Start step.
  for ($k = 1; $k <= 6; $k++) {
    // For always displaying first fieldset in active step.
    if ($k !== 1) {
      $length_fieldset = variable_get('s' . $i . '_fs' . $k . '_f');

      if ($length_fieldset == 0) {
        continue;
      }
    }
    $first_step_field = FALSE;
    $limit            = variable_get('s' . $i . '_fs' . $k . '_f');
    // If first field in first fieldset and no settings.
    if (empty($limit) && empty($length_fieldset)) {
      $limit            = 1;
      $first_step_field = TRUE;
    }

    // Start field.
    for ($y = 1; $y <= $limit; $y++) {

      // If field is active.
      if (variable_get('s' . $i . '_fs' . $k . '_f' . $y . '_active_field') !== NULL || $first_step_field === TRUE) {
        $data = isset($values[$i]['s' . $i . '_fs' . $k . '_f' . $y . '_title_field']) ? $values[$i]['s' . $i . '_fs' . $k . '_f' . $y . '_title_field'] : 0;

        // If checkbox.
        if (intval(variable_get('s' . $i . '_fs' . $k . '_f' . $y . '_type_field')) == 0) {
          if (intval($data) == 0) {
            $data = 'NO';
          }
          else {
            $data = 'YES';
          }
        }

        // If link(#murkup).
        if (intval(variable_get('s' . $i . '_fs' . $k . '_f' . $y . '_type_field')) == 3) {
          continue;
        }

        // If textfield & textarea.
        if (intval(variable_get('s' . $i . '_fs' . $k . '_f' . $y . '_type_field')) == 4 || intval(variable_get('s' . $i . '_fs' . $k . '_f' . $y . '_type_field')) == 2) {
          if (empty($data)) {
            $data = t('---');
            //continue;
          }
        }

        $details[] = [
          'field'     => variable_get('s' . $i . '_fs' . $k . '_f' . $y . '_title_field', 'Title'),
          'separator' => $separator,
          'data'      => $data,
        ];
      }
    }
  }
  return theme('confirm_details', ['details' => $details]);
}

/**
 * Confirm data for the confirm expandable step.
 */
function multistep_form_creator_confirm_details_expandable_form($form, $form_state, $i/*, $row*/) {
  $values    = isset($form_state['multistep_values'][$i][$i]) ? $form_state['multistep_values'][$i][$i] : [];
  $separator = variable_get('title_separator');
  $steps     = variable_get('admin_steps_amount', 3);
  $row       = $form_state[$i]['num_sub_form'];

  // Start step.
  foreach ($values as $key => $value) {

    for ($k = 1; $k <= $steps; $k++) {
      // For always displaying first fieldset in active step.
      if ($k !== 1) {
        $length_fieldset = variable_get('s' . $i . '_fs' . $k . '_f');

        if ($length_fieldset == 0) {
          continue;
        }
      }

      $first_step_field = FALSE;
      $limit            = variable_get('s' . $i . '_fs' . $k . '_f');
      // If first field in first fieldset and no settings.
      if (empty($limit) && empty($length_fieldset)) {
        $limit            = 1;
        $first_step_field = TRUE;
      }

      $detail = [];
      // Start field.
      for ($y = 1; $y <= $limit; $y++) {

        // If field is active.
        if (variable_get('s' . $i . '_fs' . $k . '_f' . $y . '_active_field') !== NULL || $first_step_field === TRUE) {
          $data = isset($values[$key]['fieldset_' . $k]['s' . $i . '_fs' . $k . '_f' . $y . '_title_field']) ? $values[$key]['fieldset_' . $k]['s' . $i . '_fs' . $k . '_f' . $y . '_title_field'] : 0;

          // If checkbox.
          if (intval(variable_get('s' . $i . '_fs' . $k . '_f' . $y . '_type_field')) == 0) {
            if (intval($values[$key]['fieldset_' . $k]['s' . $i . '_fs' . $k . '_f' . $y . '_title_field']) == 0) {
              $data = 'NO';
            }
            else {
              $data = 'YES';
            }
          }

          // If link(#murkup).
          if (intval(variable_get('s' . $i . '_fs' . $k . '_f' . $y . '_type_field')) == 3) {
            continue;
          }

          // If textfield & textarea.
          if (intval(variable_get('s' . $i . '_fs' . $k . '_f' . $y . '_type_field')) == 4 || intval(variable_get('s' . $i . '_fs' . $k . '_f' . $y . '_type_field')) == 2) {
            if ($values[$key]['fieldset_' . $k]['s' . $i . '_fs' . $k . '_f' . $y . '_title_field'] == '') {
              continue;
            }
          }

          $detail[] = [
            'field'     => variable_get('s' . $i . '_fs' . $k . '_f' . $y . '_title_field', 'Title'),
            'separator' => $separator,
            'data'      => $data,
          ];
        }
      }
    }
    $details[] = [
      'title'  => t('Sub form'),
      'data'   => $detail,
      'number' => $key + 1,
    ];
  }
  return theme('confirm_details_expandable', ['details' => $details]);
}
