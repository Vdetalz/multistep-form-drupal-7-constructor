<?php

/**
 * @file
 */

function steps_list() {

  $sum_steps = variable_get('admin_steps_amount', 3);

  $header = [
    ['data' => t('Step'), 'field' => 'title', 'sort' => 'ASC'],
  ];

  $row = [];

  if ($sum_steps) {

    for ($i = 1; $i <= $sum_steps; $i++) {
      $step  = l(t('Step-@step', ['@step' => $i]), 'admin/config/module-form-creator/steps/' . $i);
      $row[] = [
        ['data' => $step],
      ];
    }
  }

  $output = theme('table', [
    'header' => $header,
    'rows'   => $row,
  ]);
  $output .= theme('pager');
  return $output;
}
