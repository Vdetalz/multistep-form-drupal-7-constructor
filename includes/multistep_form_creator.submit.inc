<?php

/**
 * @file
 * Handles the form submission of the multistep form creator form.
 */

/**
 * Handles what to do with the submitted form.
 *
 * Depending on what stage has been completed.
 *
 * @see multistep_form_creator_form()
 * @see multistep_form_creator_form_validate()
 */
function multistep_form_creator_form_submit($form, &$form_state) {
  $steps   = variable_get('admin_steps_amount', 3);
  $confirm = $steps + 1;
  $finish  = $steps + 2;

  switch ($form_state['stage']) {

    case $confirm:
      $form_state['multistep_values'][$form_state['stage']] = $form_state['values'];

      for ($i = 1; $i <= $steps; $i++) {
        if ($form_state['triggering_element']['#value'] == t('Edit @form', ['@form' => variable_get('s' . $i . '_title')])) {
          $form_state['new_stage'] = $i;
        }
      }

      if ($form_state['triggering_element']['#value'] == 'Next') {
        multistep_form_creator_confirm_submit($form, $form_state);
        $form_state['complete'] = TRUE;
      }
      break;

    case $finish:
      $form_state['multistep_values'][$form_state['stage']] = $form_state['values'];
      if ($form_state['triggering_element']['#value'] != 'Back') {
        multistep_form_creator_confirm_submit($form, $form_state);
        $form_state['complete'] = TRUE;
      }
      break;

    default:
      $form_state['multistep_values'][$form_state['stage']] = $form_state['values'];
      $form_state['new_stage']                              = multistep_form_creator_move_to_next_stage($form, $form_state);
      break;

  }

  if (isset($form_state['complete'])) {
    drupal_goto('complete-page');
  }

  if ($form_state['triggering_element']['#value'] == 'Back') {
    $form_state['new_stage'] = multistep_form_creator_move_to_previous_stage($form, $form_state);
  }

  if (isset($form_state['multistep_values']['form_build_id'])) {
    $form_state['values']['form_build_id'] = $form_state['multistep_values']['form_build_id'];
  }

  $form_state['multistep_values']['form_build_id'] = $form_state['values']['form_build_id'];
  $form_state['stage']                             = $form_state['new_stage'];
  $form_state['rebuild']                           = TRUE;
}

/**
 * Handles the submission of the final stage.
 *
 * Sends an email to the user confirming their entry.
 */
function multistep_form_creator_confirm_submit($form, &$form_state) {

  $multistep_values = $form_state['multistep_values'][$form_state['stage']];

  $module = 'multistep_form_creator';
  $key    = 'multistep_form_creator_complete';
  $to     = $multistep_values['confirm']['term']['email'] . '; ' . variable_get('site_mail', 'admin@example.com');
  $from   = variable_get('site_mail', 'admin@example.com');
  $params = [
    'body'    => variable_get('message', ''),
    'subject' => 'Thank you for filling in our survey, ' . $multistep_values['confirm']['term']['email'],
  ];

  $language = language_default();
  $send     = TRUE;
  $result   = drupal_mail($module, $key, $to, $language, $params, $from, $send);
  if ($result['result'] == TRUE) {
    drupal_set_message(t('Your message has been sent.'));
  }
  else {
    drupal_set_message(t('There was a problem sending your message and it was not sent. @name', ['@name' => $multistep_values['confirm']['term']['email']]), 'error');
  }

}

/**
 * Returns what to show on the completion page.
 */
function multistep_form_creator_complete() {

  return t('Thank you for completing our survey. You have been sent an email confirming you\'ve been entered into our prize draw');

}
