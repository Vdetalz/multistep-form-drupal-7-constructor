<?php

/**
 * @file
 * Form cunfiguration of the multistep form creator module.
 */

module_load_include('inc', 'multistep_form_creator', 'includes/multistep_form_creator.admin.submit');

/**
 * MultiStep Form Creator config form.
 */
function multistep_form_creator_config_form($form, &$form_state) {
  $form = [];

  $form['message'] = [
    '#type'          => 'textfield',
    '#title'         => t('Mail message'),
    '#default_value' => variable_get('message', ''),
    '#required'      => TRUE,
  ];

  $form['title_separator'] = [
    '#type'          => 'textfield',
    '#title'         => t('Enter Separator'),
    '#default_value' => variable_get('title_separator', '-'),
    '#required'      => TRUE,
  ];

  $form['link_post_resource'] = [
    '#type'          => 'textfield',
    '#title'         => t('Link to post site'),
    '#default_value' => variable_get('link_post_resource', 'example@example.com'),
    '#required'      => TRUE,
  ];
  $form['admin_steps_amount'] = [
    '#type'          => 'select',
    '#title'         => t('Amount Steps.'),
    '#options'       => [
      1  => 1,
      2  => 2,
      3  => 3,
      4  => 4,
      5  => 5,
      6  => 6,
      7  => 7,
      8  => 8,
      9  => 9,
      10 => 10,
    ],
    '#default_value' => variable_get('admin_steps_amount', 3),
    '#description'   => t('Select amount of steps in form.'),
  ];

  $form['#submit'][] = 'multistep_form_creator_config_form_submit';

  return system_settings_form($form);
}

function multistep_form_creator_config_step_form($form, &$form_state, $i = 1) {

  $form = [];

  $form[$i] = [
    '#type'        => 'fieldset',
    '#title'       => t('Step @num Form', ['@num' => $i]),
    '#description' => t('Enter field titles and descriptions.'),
    '#collapsible' => TRUE,
    '#collapsed'   => TRUE,
  ];

  $form[$i]['s' . $i . '_expandable_step'] = [
    '#type'          => 'checkbox',
    '#title'         => t('Expandable step'),
    '#default_value' => variable_get('s' . $i . '_expandable_step', 0),
    '#states'        => [
      'disabled' => [
        ':input[name="s' . $i . '_active_step"]' => ['checked' => FALSE],
      ],
      'enabled'  => [
        ':input[name="s' . $i . '_active_step"]' => ['checked' => TRUE],
      ],
    ],

  ];

  $form[$i]['s' . $i . '_active_step_title']       = [
    '#type'          => 'checkbox',
    '#title'         => t('Show step title'),
    '#return_value'  => 1,
    '#default_value' => variable_get('s' . $i . '_active_step_title', 0),
    '#states'        => [
      'disabled' => [
        ':input[name="s' . $i . '_active_step"]' => ['checked' => FALSE],
      ],
      'enabled'  => [
        ':input[name="s' . $i . '_active_step"]' => ['checked' => TRUE],
      ],
    ],
  ];
  $form[$i]['s' . $i . '_title']                   = [
    '#type'          => 'textfield',
    '#title'         => t('Title'),
    '#description'   => t('Title step.'),
    '#default_value' => variable_get('s' . $i . '_title', 'Title Step'),
    '#states'        => [
      'invisible' => [
        ':input[name="s' . $i . '_active_step_title"]' => ['checked' => FALSE],
      ],
      'visible'   => [
        ':input[name="s' . $i . '_active_step_title"]' => ['checked' => TRUE],
      ],
    ],
  ];
  $form[$i]['s' . $i . '_active_step_description'] = [
    '#type'          => 'checkbox',
    '#title'         => t('Show step description'),
    '#default_value' => variable_get('s' . $i . '_active_step_description', 0),
  ];

  $form[$i]['s' . $i . '_description'] = [
    '#type'          => 'textfield',
    '#title'         => t('Description'),
    '#description'   => t('Description step.'),
    '#default_value' => variable_get('s' . $i . '_description', 'Description Step'),
    '#states'        => [
      'invisible' => [
        ':input[name="s' . $i . '_active_step_description"]' => ['checked' => FALSE],
      ],
      'visible'   => [
        ':input[name="s' . $i . '_active_step_description"]' => ['checked' => TRUE],
      ],
    ],
  ];

  for ($k = 1; $k <= 9; $k++) {
    $form[$i]['fieldset_' . $k] = [
      '#type'        => 'fieldset',
      '#title'       => t('Fieldset @num', ['@num' => $k]),
      '#description' => t('Enter field title and descriptions for the FIELDSET.'),
      '#collapsible' => TRUE,
      '#collapsed'   => TRUE,
    ];
    // Fieldset.
    $form[$i]['fieldset_' . $k]['s' . $i . '_fs' . $k . '_active_title_fieldset'] = [
      '#type'          => 'checkbox',
      '#title'         => t('Show fieldset title'),
      '#default_value' => variable_get('s' . $i . '_fs' . $k . '_active_title_fieldset', 0),
    ];
    $form[$i]['fieldset_' . $k]['s' . $i . '_fs' . $k . '_title_fieldset']        = [
      '#type'          => 'textfield',
      '#title'         => t('Step @num fieldset Title', ['@num' => $i]),
      '#default_value' => variable_get('s' . $i . '_fs' . $k . '_title_fieldset', 'Title Fieldset'),
      '#required'      => TRUE,
      '#states'        => [
        'invisible' => [
          ':input[name="s' . $i . '_fs' . $k . '_active_title_fieldset"]' => ['checked' => FALSE],
        ],
        'visible'   => [
          ':input[name="s' . $i . '_fs' . $k . '_active_title_fieldset"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form[$i]['fieldset_' . $k]['s' . $i . '_fs' . $k . '_active_description_fieldset'] = [
      '#type'          => 'checkbox',
      '#title'         => t('Show fieldset description'),
      '#default_value' => variable_get('s' . $i . '_fs' . $k . '_active_description_fieldset', 0),
    ];
    $form[$i]['fieldset_' . $k]['s' . $i . '_fs' . $k . '_description_fieldset']        = [
      '#type'          => 'textfield',
      '#title'         => t('Step @num fieldset Description', ['@num' => $i]),
      '#default_value' => variable_get('s' . $i . '_fs' . $k . '_description_fieldset', 'Description Fieldset'),
      '#states'        => [
        'invisible' => [
          ':input[name="s' . $i . '_fs' . $k . '_active_description_fieldset"]' => ['checked' => FALSE],
        ],
        'visible'   => [
          ':input[name="s' . $i . '_fs' . $k . '_active_description_fieldset"]' => ['checked' => TRUE],
        ],
      ],
    ];

    for ($y = 1; $y <= 3; $y++) {
      // For always display first fieldset in active field config form.
      if ($y !== 1 || $k !== 1) {
        $form[$i]['fieldset_' . $k]['s' . $i . '_fs' . $k . '_f' . $y . '_active_field'] = [
          '#type'          => 'checkbox',
          '#title'         => t('Active field @num', ['@num' => $y]),
          '#default_value' => variable_get('s' . $i . '_fs' . $k . '_f' . $y . '_active_field', 1),
          '#prefix'        => '<hr />',
        ];
      }
      $form[$i]['fieldset_' . $k]['s' . $i . '_fs' . $k . '_f' . $y . '_required']   = [
        '#type'          => 'checkbox',
        '#title'         => t('Required field'),
        '#default_value' => variable_get('s' . $i . '_fs' . $k . '_f' . $y . '_required', 0),
        '#states'        => [
          'disabled' => [
            ':input[name="s' . $i . '_fs' . $k . '_f' . $y . '_active_field"]' => ['checked' => FALSE],
          ],
          'enabled'  => [
            ':input[name="s' . $i . '_fs' . $k . '_f' . $y . '_active_field"]' => ['checked' => TRUE],
          ],
        ],
      ];
      $form[$i]['fieldset_' . $k]['s' . $i . '_fs' . $k . '_f' . $y . '_type_field'] = [
        '#type'          => 'select',
        '#title'         => t('Field @num Type', ['@num' => $y]),
        '#options'       => variable_get('types_fields'),
        '#default_value' => ($y !== 1) ? variable_get('s' . $i . '_fs' . $k . '_f' . $y . '_type_field', '0') : variable_get('s' . $i . '_fs' . $k . '_f' . $y . '_type_field', '1'),
        '#description'   => t('Select type of the field.'),
        '#states'        => [
          'disabled' => [
            ':input[name="s' . $i . '_fs' . $k . '_f' . $y . '_active_field"]' => ['checked' => FALSE],
          ],
          'enabled'  => [
            ':input[name="s' . $i . '_fs' . $k . '_f' . $y . '_active_field"]' => ['checked' => TRUE],
          ],
        ],
      ];

      $form[$i]['fieldset_' . $k]['s' . $i . '_fs' . $k . '_f' . $y . '_show_title_field'] = [
        '#type'          => 'checkbox',
        '#title'         => t('Show title'),
        '#default_value' => variable_get('s' . $i . '_fs' . $k . '_f' . $y . '_show_title_field', 1),
        '#states'        => [
          'disabled' => [
            ':input[name="s' . $i . '_fs' . $k . '_f' . $y . '_active_field"]' => ['checked' => FALSE],
          ],
          'enabled'  => [
            ':input[name="s' . $i . '_fs' . $k . '_f' . $y . '_active_field"]' => ['checked' => TRUE],
          ],
        ],
      ];

      $form[$i]['fieldset_' . $k]['s' . $i . '_fs' . $k . '_f' . $y . '_title_field'] = [
        '#type'          => 'textfield',
        '#title'         => t('Field @num Title', ['@num' => $y]),
        '#default_value' => variable_get('s' . $i . '_fs' . $k . '_f' . $y . '_title_field', 'Title Field'),
        '#required'      => TRUE,
        '#states'        => [
          'invisible' => [
            ':input[name="s' . $i . '_fs' . $k . '_f' . $y . '_show_title_field"]' => ['checked' => FALSE],
          ],
          'visible'   => [
            ':input[name="s' . $i . '_fs' . $k . '_f' . $y . '_show_title_field"]' => ['checked' => TRUE],
          ],
          'disabled'  => [
            ':input[name="s' . $i . '_fs' . $k . '_f' . $y . '_active_field"]' => ['checked' => FALSE],
          ],
          'enabled'   => [
            ':input[name="s' . $i . '_fs' . $k . '_f' . $y . '_active_field"]' => ['checked' => TRUE],
          ],

        ],
      ];

      $form[$i]['fieldset_' . $k]['s' . $i . '_fs' . $k . '_f' . $y . '_show_description_field'] = [
        '#type'          => 'checkbox',
        '#title'         => t('Show description'),
        '#default_value' => variable_get('s' . $i . '_fs' . $k . '_f' . $y . '_show_description_field', 0),
        '#states'        => [
          'disabled' => [
            ':input[name="s' . $i . '_fs' . $k . '_f' . $y . '_active_field"]' => ['checked' => FALSE],
          ],
          'enabled'  => [
            ':input[name="s' . $i . '_fs' . $k . '_f' . $y . '_active_field"]' => ['checked' => TRUE],
          ],
        ],
      ];
      $form[$i]['fieldset_' . $k]['s' . $i . '_fs' . $k . '_f' . $y . '_description_field']      = [
        '#type'          => 'textfield',
        '#title'         => t('Field @num Description', ['@num' => $y]),
        '#default_value' => variable_get('s' . $i . '_fs' . $k . '_f' . $y . '_description_field', 'Description Field'),
        '#states'        => [
          'invisible' => [
            ':input[name="s' . $i . '_fs' . $k . '_f' . $y . '_show_description_field"]' => ['checked' => FALSE],
          ],
          'visible'   => [
            ':input[name="s' . $i . '_fs' . $k . '_f' . $y . '_show_description_field"]' => ['checked' => TRUE],
          ],
          'disabled'  => [
            ':input[name="s' . $i . '_fs' . $k . '_f' . $y . '_active_field"]' => ['checked' => FALSE],
          ],
          'enabled'   => [
            ':input[name="s' . $i . '_fs' . $k . '_f' . $y . '_active_field"]' => ['checked' => TRUE],
          ],

        ],
      ];
    }
  }

  $form['#submit'][] = 'multistep_form_creator_config_form_submit';

  return system_settings_form($form);
}
