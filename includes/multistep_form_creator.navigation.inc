<?php

/**
 * @file
 * Handles the navigation of the multistep form creator form.
 *
 * All hooks are in the .module file.
 */

/**
 * Generate a header which informs the user of which stage they're on.
 */
function multistep_form_creator_get_header($form, &$form_state) {

  $form_state['stage'] = isset($form_state['stage']) ? $form_state['stage'] : 1;

  $steps        = variable_get('admin_steps_amount', 3);
  $stages       = [];
  $confirm_page = $steps + 1;
  $finish_page  = $steps + 2;

  if ($form_state['stage'] == 'confirm') {
    $form_state['stage'] = $confirm_page;
  }

  if ($form_state['stage'] == 'finish') {
    $form_state['stage'] = $finish_page;
  }

  for ($i = 1; $i <= $steps; $i++) {
    $form_stages[$i] = $i;
    $stages[$i]      = ['data' => variable_get('s' . $i . '_title')];

    // Set the confirm step.
    if ($i == $steps) {
      $form_stages[$i + 1] = $i + 1;
      $stages[$i + 1]      = ['data' => 'Confirm'];
    }
  }

  if (isset($form_stages[$form_state['stage']])) {
    $current_step = $form_stages[$form_state['stage']];
  }
  else {
    $current_step = 1;
  }

  $stages[$current_step]['class'] = ['active'];

  $stages_list = theme('item_list', ['items' => $stages]);

  $form['header'] = [
    '#type'  => 'fieldset',
    '#title' => '',
    '#value' => $stages_list,
  ];
  return $form;
}

/**
 * Given the current stage the user is on, calculate what the next step would be.
 */
function multistep_form_creator_move_to_next_stage($form, &$form_state) {

  return $form_state['stage'] + 1;

}

/**
 * Given the current stage the user is on.
 *
 * Calculate what the previous step would be.
 */
function multistep_form_creator_move_to_previous_stage($form, &$form_state) {

  return $form_state['stage'] - 1;
}
