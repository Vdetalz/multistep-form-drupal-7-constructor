<?php

/**
 * @file
 * Handles the form validation of the multistep form creator form.
 *
 * All hooks are in the .module file.
 */

/**
 * Master validation function for the multistep form creator form.
 *
 * Uses per-stage validation and calls functions for each one.
 */
function multistep_form_creator_form_validate($form, &$form_state) {

  if ($form_state['triggering_element']['#value'] == 'Back') {
    return;
  }
  $steps = variable_get('admin_steps_amount', 3);

  $confirm = $steps + 1;
  $finish  = $steps + 2;

  switch ($form_state['stage']) {

    case $confirm:
      return multistep_form_creator_confirm_validate($form, $form_state);
  }
}

/**
 * Validation for the winery_details step.
 */
function multistep_form_creator_step_validate($form, &$form_state) {

  foreach ($form_state['values'] as $value => $name) {

    if (!$name) {
      $field_name = $form_state['stage']['winery_details'][$value]['#title'];
      form_set_error($value, "You must fill out '$field_name' before continuing");
    }
  }
}

/**
 * Validation for the confirm step.
 */
function multistep_form_creator_confirm_validate($form, &$form_state) {
  if (!valid_email_address($form_state['values']['confirm']['term']['email']) && $form_state['triggering_element']['#value'] == 'Next') {
    form_set_error('confirm', "You must enter correct email before continuing");
  }
}
