<?php

/**
 * @file
 * Form submission of the multistep form creator module.
 */

/**
 * MultiStep Form Creator submit form.
 */
function multistep_form_creator_config_form_submit($form, &$form_state) {

  $sum_steps = variable_get('admin_steps_amount', 3);

  // Amount fields in the current fieldset.
  $count_fields = 0;

  for ($i = 1; $i <= $sum_steps; $i++) {

    for ($k = 1; $k <= 9; $k++) {

      for ($y = 1; $y <= 3; $y++) {

        if (isset($form_state['values']['s' . $i . '_fs' . $k . '_f' . $y . '_active_field']) && $form_state['values']['s' . $i . '_fs' . $k . '_f' . $y . '_active_field'] == '1') {
          $count_fields++;
        }
        // Required or not.
        if (isset($form_state['values']['s' . $i . '_fs' . $k . '_f' . $y . '_required']) && $form_state['values']['s' . $i . '_fs' . $k . '_f' . $y . '_required'] == '1') {
          variable_set('s' . $i . '_fs' . $k . '_f' . $y . '_required', TRUE);
        }
        else {
          variable_set('s' . $i . '_fs' . $k . '_f' . $y . '_required', FALSE);
        }
      }
      // Amount fields in the current fieldset.
      if ($k !== 1) {
        variable_set('s' . $i . '_fs' . $k . '_f', $count_fields);
      }
      $count_fields = 0;
    }
  }
  // Expandable fieldset or no.
  if (isset($form_state['values']['s' . $i . '_expandable_step'])) {
    variable_set('s' . $i . '_expandable_step', $form_state['values']['s' . $i . '_expandable_step']);
  }

  if (isset($form_state['admin_steps_amount'])) {
    variable_set('admin_steps_amount', $form_state['admin_steps_amount']);
  }
  drupal_flush_all_caches();
}
