<?php

/**
 * @file
 * Handles the form elements of the multistep form creator.
 *
 * All hooks are in the .module file.
 */

/**
 * Master form which calls an individual form for each step.
 *
 * @see multistep_form_creator_form_validate()
 * @see multistep_form_creator_form_submit()
 */
function multistep_form_creator_form($form, &$form_state) {

  drupal_add_css(drupal_get_path('module', 'multistep_form_creator') . '/css/multistep_form_creator.css');

  if (!isset($form_state['stage'])) {
    $form_state['stage'] = '1';
  }
  $steps         = variable_get('admin_steps_amount', 3);
  $confirm       = $steps + 1;
  $finish        = $steps + 2;
  $is_expandable = variable_get('s' . $form_state['stage'] . '_expandable_step');

  $form = [];
  $form = multistep_form_creator_get_header($form, $form_state);

  $values = isset($form_state['multistep_values'][$form_state['stage']]) ? $form_state['multistep_values'][$form_state['stage']] : [];
  // If 'confirm' stage.
  if ($form_state['stage'] == $confirm) {
    return multistep_form_creator_confirm_form($form, $form_state);
  }

  $types_fields = variable_get('types_fields');

  // If step is expandable.
  if ($is_expandable === 1) {
    $form['#tree'] = TRUE;

    //$values = isset($form_state['multistep_values'][$form_state['stage']]) ? $form_state['multistep_values'][$form_state['stage']] : array();

    $form[$form_state['stage']] = [
      '#type'        => 'fieldset',
      '#title'       => variable_get('s' . $form_state['stage'] . '_active_step_title') ? variable_get('s' . $form_state['stage'] . '_title') : '',
      '#description' => variable_get('s' . $form_state['stage'] . '_active_step_description') ? variable_get('s' . $form_state['stage'] . '_description') : '',
      '#prefix'      => '<div id="replace-this">',
      '#suffix'      => '</div>',
    ];

    if (!array_key_exists('clicked_button', $form_state)) {
      multistep_form_creator_expandable_form($form, $form_state, 0, $values);
    }
    else {
      if (isset($values[$form_state['stage']])) {
        foreach ($values[$form_state['stage']] as $rows => $value) {
          multistep_form_creator_expandable_form($form, $form_state, $rows, $values);
        }
      }

      $last = isset($form_state['values'][$form_state['stage']]) ? count($form_state['values'][$form_state['stage']]) : 0;
      multistep_form_creator_expandable_form($form, $form_state, $last, $values);
    }

    if ($form_state['stage'] != '1') {
      $form['back'] = [
        '#type'  => 'submit',
        '#value' => t('Back'),
      ];
    }

    $form['another'] = [
      '#type'     => 'submit',
      '#value'    => t('Add more'),
      '#validate' => ['multistep_form_creator_expandable_form_another'],
      '#ajax'     => [
        'callback' => 'ajax_simplest_callback',
        'wrapper'  => 'replace-this',
        'method'   => 'replace',
        'effect'   => 'fade',
      ],
    ];

    $form['next'] = [
      '#type'  => 'submit',
      '#value' => t('Next'),
    ];
    return $form;
  }
  // If step is not expandable.
  else {
    //$values = isset($form_state['multistep_values'][$form_state['stage']]) ? $form_state['multistep_values'][$form_state['stage']] : array();

    $form[$form_state['stage']] = [
      '#type'        => 'fieldset',
      '#title'       => variable_get('s' . $form_state['stage'] . '_active_step_title') ? variable_get('s' . $form_state['stage'] . '_title') : '',
      '#description' => variable_get('s' . $form_state['stage'] . '_active_step_description') ? variable_get('s' . $form_state['stage'] . '_description') : '',
    ];

    // Start fisldset.
    for ($k = 1; $k <= 9; $k++) {
      // For always displaying first fieldset in active step.
      if ($k !== 1) {
        $length_fieldset = variable_get('s' . $form_state['stage'] . '_fs' . $k . '_f');

        if (intval($length_fieldset) == 0) {
          continue;
        }
      }

      $form[$form_state['stage']]['fieldset_' . $k] = [
        '#type'        => 'fieldset',
        '#title'       => variable_get('s' . $form_state['stage'] . '_fs' . $k . '_active_title_fieldset') ? variable_get('s' . $form_state['stage'] . '_fs' . $k . '_title_fieldset') : '',
        '#description' => variable_get('s' . $form_state['stage'] . '_fs' . $k . '_active_description_fieldset') ? variable_get('s' . $form_state['stage'] . '_fs' . $k . '_description_fieldset') : '',
      ];

      $limit = variable_get('s' . $form_state['stage'] . '_fs' . $k . '_f', '3');

      // If first field in first fieldset and no settings.
      if (empty($limit) && empty($length_fieldset)) {
        $limit = 1;
      }

      // Start fields.
      for ($y = 1; $y <= $limit; $y++) {

        if (variable_get('s' . $form_state['stage'] . '_fs' . $k . '_f' . $y . '_active_field') !== NULL || $y === 1) {

          // If type_field is radio.
          if (intval(variable_get('s' . $form_state['stage'] . '_fs' . $k . '_f' . $y . '_type_field')) === 1) {
            $options = [
              0 => t('Off'),
              1 => t('On'),
            ];

            $form[$form_state['stage']]['fieldset_' . $k]['s' . $form_state['stage'] . '_fs' . $k . '_f' . $y . '_title_field'] = [
              '#type'          => $types_fields[variable_get('s' . $form_state['stage'] . '_fs' . $k . '_f' . $y . '_type_field')],
              '#title'         => variable_get('s' . $form_state['stage'] . '_fs' . $k . '_f' . $y . '_title_field'),
              '#description'   => variable_get('s' . $form_state['stage'] . '_fs' . $k . '_f' . $y . '_description_field'),
              '#default_value' => isset($values['s' . $form_state['stage'] . '_fs' . $k . '_f' . $y . '_title_field']) ? $values['s' . $form_state['stage'] . '_fs' . $k . '_f' . $y . '_title_field'] : 0,
              '#options'       => $options,
            ];
            continue;
          }

          // If type_field is link.
          if (intval(variable_get('s' . $form_state['stage'] . '_fs' . $k . '_f' . $y . '_type_field')) === 3) {
            $form[$form_state['stage']][$row]['fieldset_' . $k]['s' . $form_state['stage'] . '_fs' . $k . '_f' . $y . '_title_field'] = [
              '#markup' => '<p><a href="' . variable_get('link_post_resource', 'example@example.com') . '">' . variable_get('s' . $form_state['stage'] . '_fs' . $k . '_f' . $y . '_title_field') . '</a><p>'
                           . '<p>' . variable_get('s' . $form_state['stage'] . '_fs' . $k . '_f' . $y . '_description_field') . '</p>',
            ];
            continue;
          }

          $form[$form_state['stage']]['fieldset_' . $k]['s' . $form_state['stage'] . '_fs' . $k . '_f' . $y . '_title_field'] = [
            '#type'          => $types_fields[variable_get('s' . $form_state['stage'] . '_fs' . $k . '_f' . $y . '_type_field')],
            '#title'         => variable_get('s' . $form_state['stage'] . '_fs' . $k . '_f' . $y . '_title_field'),
            '#description'   => variable_get('s' . $form_state['stage'] . '_fs' . $k . '_f' . $y . '_description_field'),
            '#default_value' => isset($values['s' . $form_state['stage'] . '_fs' . $k . '_f' . $y . '_title_field']) ? $values['s' . $form_state['stage'] . '_fs' . $k . '_f' . $y . '_title_field'] : NULL,
            '#required'      => variable_get('s' . $form_state['stage'] . '_fs' . $k . '_f' . $y . '_required', FALSE),
          ];
        }
      }
    }

    if ($form_state['stage'] != '1') {
      $form['back'] = [
        '#type'  => 'submit',
        '#value' => t('Back'),
      ];
    }

    $form['next'] = [
      '#type'  => 'submit',
      '#value' => t('Next'),
    ];

    return $form;
  }
}

/**
 * Data for the expandable type of the forms.
 */
function multistep_form_creator_expandable_form(&$form, $form_state, $rows = 0, $values = []) {
  $types_fields = variable_get('types_fields');

  if (empty($form_state[$form_state['stage']]['num_sub_form'])) {
    $form_state[$form_state['stage']]['num_sub_form'] = 0;
  }

  $rows = $form_state[$form_state['stage']]['num_sub_form'];
  // Start additional subform.
  for ($row = 0; $row <= $rows; $row++) {
    // Start fieldset.
    for ($k = 1; $k <= 9; $k++) {
      if ($k !== 1) {

        $length_fieldset = variable_get('s' . $form_state['stage'] . '_fs' . $k . '_f');

        if (intval($length_fieldset) == 0) {
          continue;
        }
      }

      $form[$form_state['stage']][$row]['fieldset_' . $k] = [
        '#type'        => 'fieldset',
        '#title'       => variable_get('s' . $form_state['stage'] . '_fs' . $k . '_active_title_fieldset') ? variable_get('s' . $form_state['stage'] . '_fs' . $k . '_title_fieldset') : '',
        '#description' => variable_get('s' . $form_state['stage'] . '_fs' . $k . '_active_description_fieldset') ? variable_get('s' . $form_state['stage'] . '_fs' . $k . '_description_fieldset') : '',
      ];

      $limit = variable_get('s' . $form_state['stage'] . '_fs' . $k . '_f', '3');
      // If first field in first fieldset and no settings.
      if (empty($limit) && empty($length_fieldset)) {
        $limit = 1;
      }

      // Start field.
      for ($y = 1; $y <= $limit; $y++) {

        if (variable_get('s' . $form_state['stage'] . '_fs' . $k . '_f' . $y . '_active_field') !== NULL || $y === 1) {
          // If type_field is radio.
          if (intval(variable_get('s' . $form_state['stage'] . '_fs' . $k . '_f' . $y . '_type_field')) === 1) {
            $options = [
              0 => t('Off'),
              1 => t('On'),
            ];

            $form[$form_state['stage']][$row]['fieldset_' . $k]['s' . $form_state['stage'] . '_fs' . $k . '_f' . $y . '_title_field'] = [
              '#type'          => $types_fields[variable_get('s' . $form_state['stage'] . '_fs' . $k . '_f' . $y . '_type_field')],
              '#title'         => variable_get('s' . $form_state['stage'] . '_fs' . $k . '_f' . $y . '_title_field'),
              '#description'   => variable_get('s' . $form_state['stage'] . '_fs' . $k . '_f' . $y . '_description_field'),
              '#default_value' => isset($values[$form_state['stage']][$row]['fieldset_' . $k]['s' . $form_state['stage'] . '_fs' . $k . '_f' . $y . '_title_field']) ? $values[$form_state['stage']][$row]['fieldset_' . $k]['s' . $form_state['stage'] . '_fs' . $k . '_f' . $y . '_title_field'] : 0,
              '#options'       => $options,
            ];
            continue;
          }

          // If not link field_type.
          if (intval(variable_get('s' . $form_state['stage'] . '_fs' . $k . '_f' . $y . '_type_field')) === 3) {
            $form[$form_state['stage']][$row]['fieldset_' . $k]['s' . $form_state['stage'] . '_fs' . $k . '_f' . $y . '_title_field'] = [
              '#markup' => '<p><a href="' . variable_get('link_post_resource', 'example@example.com') . '">' . variable_get('s' . $form_state['stage'] . '_fs' . $k . '_f' . $y . '_title_field') . '</a><p>' . '<p>' . variable_get('s' . $form_state['stage'] . '_fs' . $k . '_f' . $y . '_description_field') . '</p>',
            ];
            continue;
          }

          $form[$form_state['stage']][$row]['fieldset_' . $k]['s' . $form_state['stage'] . '_fs' . $k . '_f' . $y . '_title_field'] = [
            '#type'          => $types_fields[variable_get('s' . $form_state['stage'] . '_fs' . $k . '_f' . $y . '_type_field')],
            '#title'         => variable_get('s' . $form_state['stage'] . '_fs' . $k . '_f' . $y . '_title_field'),
            '#description'   => variable_get('s' . $form_state['stage'] . '_fs' . $k . '_f' . $y . '_description_field'),
            '#default_value' => isset($values[$form_state['stage']][$row]['fieldset_' . $k]['s' . $form_state['stage'] . '_fs' . $k . '_f' . $y . '_title_field']) ? $values[$form_state['stage']][$row]['fieldset_' . $k]['s' . $form_state['stage'] . '_fs' . $k . '_f' . $y . '_title_field'] : NULL,
            '#required'      => variable_get('s' . $form_state['stage'] . '_fs' . $k . '_f' . $y . '_required', FALSE),
          ];
        }
      }
    }
  }
}

/**
 * Additional form of the contacts_name stage.
 */
function multistep_form_creator_expandable_form_another($form, &$form_state) {
  $form_state[$form_state['stage']]['num_sub_form']++;
  $form_state['storage']['another'] = TRUE;
  $form_state['rebuild']            = TRUE;
}

/**
 * Ajax handler of the contacts_name step.
 */
function ajax_simplest_callback($form, $form_state) {
  return $form[$form_state['stage']];
}

/**
 * Form for the confirm step.
 *
 * @see multistep_form_creator_form()
 */
function multistep_form_creator_confirm_form($form, &$form_state) {

  $values = isset($form_state['multistep_values'][$form_state['stage']]) ? $form_state['multistep_values'][$form_state['stage']] : [];

  $form['confirm'] = [
    '#type'        => 'fieldset',
    '#title'       => t('Confirm Details'),
    '#description' => t('Please checkall of your details carefully before confirming your winery submission. Enter details foreach wine you will be submitting'),
    '#tree'        => TRUE,
  ];

  $steps = variable_get('admin_steps_amount', 3);

  // Start the information about completed steps.
  for ($i = 1; $i <= $steps; $i++) {
    $form['confirm'][$i] = [
      '#type'  => 'fieldset',
      '#title' => variable_get('s' . $i . '_title', 'Step-' . $i),
      '#tree'  => TRUE,
    ];
    // If is expandable step.
    if (isset($form_state[$i]['num_sub_form'])) {
      $row                            = $form_state[$i]['num_sub_form'];
      $form['confirm'][$i]['details'] = [
        '#markup' => multistep_form_creator_confirm_details_expandable_form($form, $form_state, $i/*, $row*/),
      ];
    }
    else {
      $form['confirm'][$i]['details'] = [
        '#markup' => multistep_form_creator_confirm_details_form($form, $form_state, $i),
      ];
    }
    $form['confirm'][$i]['edit'] = [
      '#type'  => 'submit',
      '#value' => t('Edit @form', ['@form' => variable_get('s' . $i . '_title', 'Step-' . $i)]),
    ];
  }

  // Form confirm.
  $form['confirm']['term']                 = [
    '#type'  => 'fieldset',
    '#title' => t('Terms'),
    '#tree'  => TRUE,
  ];
  $form['confirm']['term']['term_details'] = [
    '#type'          => 'checkbox',
    '#title'         => t("I/we accept the 'Submission Terms'"),
    '#default_value' => isset($values['term']['term_details']) ? $values['term']['term_details'] : NULL,
    '#ajax'          => [
      'callback' => 'ajax_term_callback',
      'wrapper'  => 'multistep-form-creator-form',
      'method'   => 'replace',
      'effect'   => 'fade',
    ],
  ];
  $form['confirm']['term']['email']        = [
    '#type'          => 'textfield',
    '#title'         => t("Email"),
    '#description'   => t('Enter email address'),
    '#default_value' => isset($values['term']['email']) ? $values['term']['email'] : NULL,
  ];

  $form['back'] = [
    '#type'  => 'submit',
    '#value' => t('Back'),
  ];

  $form['next'] = [
    '#type'   => 'submit',
    '#value'  => t('Next'),
    '#states' => [
      'disabled' => [
        ':input[name="confirm[term][term_details]"]' => ['checked' => FALSE],
      ],
      'enabled'  => [
        ':input[name="confirm[term][term_details]"]' => ['checked' => TRUE],
      ],
    ],
  ];
  return $form;
}

/**
 * Ajax handler.
 */
function ajax_term_callback($form, &$form_state) {

  $form_state['rebuild'] = TRUE;
  return $form;
}

/**
 * Form for the finished step.
 *
 * @see multistep_form_creator_form()
 */
function multistep_form_creator_finished_form($form, &$form_state) {

  $values = isset($form_state['multistep_values']['finished']) ? $form_state['multistep_values']['finished'] : [];
  multistep_form_creator_confirm_submit($form, $form_state);
  drupal_set_message(multistep_form_creator_complete());

  $form['return'] = [
    '#type'  => 'submit',
    '#value' => t('Return to Home Page'),
  ];

  return $form;
}
