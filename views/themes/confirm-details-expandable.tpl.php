<?php

/**
 * @file
 * Displays the confirm winery details.
 *
 * Available variables:
 * - $details: An array of the form_state to display.
 * - $details contains of:
 *  - title,
 *  - number,
 *  - data(array):
 *    -data,
 *    -separator,
 *    -field.
 */
?>
<?php for ($i = 0; $i < count($details); $i++): ?>
  <table>
    <p><?php echo $details[$i]['number'] . ' - ' . $details[$i]['title'] ?></p>
    <?php for ($k = 0; $k < count($details[$i]['data']); $k++): ?>
      <tr>
        <td><strong><?php echo $details[$i]['data'][$k]['field'];
            echo $details[$i]['data'][$k]['separator']; ?></strong></td>
        <td><?php echo $details[$i]['data'][$k]['data']; ?></td>
      </tr>
    <?php endfor; ?>
  </table>
<?php endfor; ?>
<hr/>
